﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weather
{
   
        public class Coord
        {

            [JsonProperty("lon")]
            public double lon { get; set; }

            [JsonProperty("lat")]
            public double lat { get; set; }
        }

        public class Example
        {

            [JsonProperty("id")]
            public int id { get; set; }

            [JsonProperty("name")]
            public string name { get; set; }

            [JsonProperty("country")]
            public string country { get; set; }

            [JsonProperty("coord")]
            public Coord coord { get; set; }
        }
    }

