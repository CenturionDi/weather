﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Weather
{
    public class Forecast
    {
        public string Description { get; set; }
        public int ID { get; set; }
        public string IconID { get; set; }
        public DateTime Date { get; set; }
        public string WindType { get; set; }
        public string WindDirection { get; set; }
        public string WindSpeed { get; set; }
        public string MaxTemperature { get; set; }
        public string MinTemperature { get; set; }
        public string Pressure { get; set; }
        public string Humidity { get; set; }
        public BitmapImage Image { get; set; }
    }
}
