﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Linq;

namespace Weather
{
    
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }
       
        private const string API_KEY = "d1607879a9f17819e95cf6205a44e3d9";

       
        private const string CurrentUrl =
            "http://api.openweathermap.org/data/2.5/weather?" +
            "q=@LOC@&mode=xml&units=metric&APPID=" + API_KEY;
        private const string ForecastUrl =
            "http://api.openweathermap.org/data/2.5/forecast?" +
            "q=@LOC@&mode=xml&units=metric&APPID=" + API_KEY;
        Model md = new Model();
        private void Location_TextChanged(object sender, TextChangedEventArgs e)
        {
           
           
        }

        private void DisplayForecast(string xml)
        {

            XmlDocument xml_doc = new XmlDocument();
            xml_doc.LoadXml(xml);
            XmlNode loc_node = xml_doc.SelectSingleNode("weatherdata/location");
            txtCity.Text = loc_node.SelectSingleNode("name").InnerText;
            txtCountry.Text = loc_node.SelectSingleNode("country").InnerText;
            XmlNode geo_node = loc_node.SelectSingleNode("location");
            //txtLat.Text = geo_node.Attributes["latitude"].Value;
            //txtLong.Text = geo_node.Attributes["longitude"].Value;
            //txtId.Text = geo_node.Attributes["geobaseid"].Value;

            //WeatherList.Items.Clear();
            List<Forecast> templ = new List<Forecast>();

            foreach (XmlNode time_node in xml_doc.SelectNodes("//time"))
            {
                templ.Add(new Forecast
                {
                    Description = time_node.SelectSingleNode("symbol").Attributes["name"].Value,
                    ID = int.Parse(time_node.SelectSingleNode("symbol").Attributes["number"].Value),
                    IconID = time_node.SelectSingleNode("symbol").Attributes["var"].Value,
                    Date = DateTime.Parse(time_node.Attributes["from"].Value, null, DateTimeStyles.AssumeUniversal),
                    WindType = time_node.SelectSingleNode("windSpeed").Attributes["name"].Value,
                    WindSpeed = time_node.SelectSingleNode("windSpeed").Attributes["mps"].Value,
                    WindDirection = time_node.SelectSingleNode("windDirection").Attributes["code"].Value,
                    MaxTemperature = time_node.SelectSingleNode("temperature").Attributes["max"].Value,
                    MinTemperature = time_node.SelectSingleNode("temperature").Attributes["min"].Value,
                    Pressure = time_node.SelectSingleNode("pressure").Attributes["value"].Value,
                    Humidity = time_node.SelectSingleNode("humidity").Attributes["value"].Value
                    
                });
                
                
            }
            

            var pack = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "/WeatherIcons/";
            var img = string.Empty;
            foreach (var item in templ)
            {
                if (item.ID >= 200 && item.ID < 300) img = "thunderstorm.png";
                else if (item.ID >= 300 && item.ID < 500) img = "drizzle.png";
                else if (item.ID >= 500 && item.ID < 600) img = "rain.png";
                else if (item.ID >= 600 && item.ID < 700) img = "snow.png";
                else if (item.ID >= 700 && item.ID < 800) img = "atmosphere.png";
                else if (item.ID == 800) img = ((item.IconID) == "d") ? "clear_day.png" : "clear_night.png";
                else if (item.ID == 801) img = ((item.IconID) == "d") ? "few_clouds_day.png" : "few_clouds_night.png";
                else if (item.ID == 802 || item.ID == 803) img = ((item.IconID) == "d") ? "broken_clouds_day.png" : "broken_clouds_night.png";
                else if (item.ID == 804) img = "overcast_clouds.png";
                else if (item.ID >= 900 && item.ID < 903) img = "extreme.png";
                else if (item.ID == 903) img = "cold.png";
                else if (item.ID == 904) img = "hot.png";
                else if (item.ID == 905 || item.ID >= 951) img = "windy.png";
                else if (item.ID == 906) img = "hail.png";

                Uri source = new Uri(pack + img);

                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.UriSource = source;
                bmp.EndInit();
                
                item.Image = bmp;
            }
            WeatherList.ItemsSource = templ;
            
        }
        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            bool found = false;
            var border = (resultStack.Parent as ScrollViewer).Parent as Border;
            
            var data = md.GetData();

            string query = (sender as TextBox).Text;

            if (query.Length == 0)
            {
                // Clear   
                resultStack.Children.Clear();
                border.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                border.Visibility = System.Windows.Visibility.Visible;
            }

            // Clear the list   
            resultStack.Children.Clear();

            // Add the result   
            foreach (var obj in data)
            {
                if (obj.ToLower().StartsWith(query.ToLower()))
                {
                    // The word starts with this... Autocomplete must work   
                    addItem(obj);
                    found = true;
                }
            }

            if (!found)
            {
                resultStack.Children.Add(new TextBlock() { Text = "No results found." });
            }
        }

        private void addItem(string text)
        {
            TextBlock block = new TextBlock();

            // Add the text   
            block.Text = text;

            // A little style...   
            block.Margin = new Thickness(2, 3, 2, 3);
            block.Cursor = Cursors.Hand;

            // Mouse events   
            block.MouseLeftButtonUp += (sender, e) =>
            {
                Location.Text = (sender as TextBlock).Text;
            };

            block.MouseEnter += (sender, e) =>
            {
                TextBlock b = sender as TextBlock;
                b.Background = Brushes.PeachPuff;
            };

            block.MouseLeave += (sender, e) =>
            {
                TextBlock b = sender as TextBlock;
                b.Background = Brushes.Transparent;
            };

            // Add to the panel   
            resultStack.Children.Add(block);
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {

            string url = ForecastUrl.Replace("@LOC@", Location.Text);

            using (WebClient client = new WebClient())
            {

                try
                {
                    DisplayForecast(client.DownloadString(url));
                }
                catch (WebException ex)
                {
                    //DisplayError(ex);
                }
                //catch (Exception ex)
                //{
                //    MessageBox.Show("Unknown error\n" + ex.Message);
                //}
            }
        }
        //private BitmapImage image(List<Forecast> templ)
        //{
        //    var pack = "C:/Users/abdim/source/repos/Weather/Weather/WeatherIcons/";
        //    var img = string.Empty;

        //    foreach (var item in templ) 
        //    {
        //        if (item.ID >= 200 && item.ID < 300) img = "thunderstorm.png";
        //        else if (item.ID >= 300 && item.ID < 500) img = "drizzle.png";
        //        else if (item.ID >= 500 && item.ID < 600) img = "rain.png";
        //        else if (item.ID >= 600 && item.ID < 700) img = "snow.png";
        //        else if (item.ID >= 700 && item.ID < 800) img = "atmosphere.png";
        //        else if (item.ID == 800) img = ((item.IconID) == "d") ? "clear_day.png" : "clear_night.png";
        //        else if (item.ID == 801) img = ((item.IconID) == "d") ? "few_clouds_day.png" : "few_clouds_night.png";
        //        else if (item.ID == 802 || item.ID == 803) img = ((item.IconID) == "d") ? "broken_clouds_day.png" : "broken_clouds_night.png";
        //        else if (item.ID == 804) img = "overcast_clouds.png";
        //        else if (item.ID >= 900 && item.ID < 903) img = "extreme.png";
        //        else if (item.ID == 903) img = "cold.png";
        //        else if (item.ID == 904) img = "hot.png";
        //        else if (item.ID == 905 || item.ID >= 951) img = "windy.png";
        //        else if (item.ID == 906) img = "hail.png";

        //        Uri source = new Uri(pack + img);

        //        BitmapImage bmp = new BitmapImage();
        //        bmp.BeginInit();
        //        bmp.UriSource = source;
        //        bmp.EndInit();
        //        return bmp;
        //    }
        //    BitmapImage er = new BitmapImage();
        //    return er;
        //}
    }
}
