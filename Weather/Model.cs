﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Weather
{
    
    public class Model
    {
        public List<Example> cities22;
        public Model()
        {
            using (StreamReader r = new StreamReader("city.list.json"))
            {
                
                List<Example> cities2 = new List<Example>();
                string json = r.ReadToEnd();
                cities2 = JsonConvert.DeserializeObject<List<Example>>(json);
                
                cities22 = cities2;
            }
        }

        public List<string> GetData()
        {
            
            List<string> cities = new List<string>();
            foreach (Example item in cities22)
                {
                    cities.Add(item.name);
                }
                return cities;
            


            //List<string> cities = new List<string>();

            //    cities.Add("Astana");
            //    cities.Add("Almaty");
            //    cities.Add("Aktay");
            //    cities.Add("Alay");
            //    cities.Add("Asta");
            //    cities.Add("Asdsd");
            //    cities.Add("Altamyty");
            //    cities.Add("Zexyndr");
            //    cities.Add("Blue");

            //    return cities;
            //}
        }
    }
}
